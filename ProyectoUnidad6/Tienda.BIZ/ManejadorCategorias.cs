﻿using Tienda.COMMON.Interfaces;
using Tienda.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Tienda.BIZ
{
    public class ManejadorCategorias : IManejadorCategorias
    {
        IRepositorio<Categoria> repositorio;
        public ManejadorCategorias(IRepositorio<Categoria> repo)
        {
            repositorio = repo;
        }

        public List<Categoria> Listar => repositorio.Read;

        public bool Agregar(Categoria entidad)
        {
            return repositorio.Create(entidad);
        }

        public Categoria BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Categoria entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
