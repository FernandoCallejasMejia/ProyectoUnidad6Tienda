﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;

namespace Tienda.BIZ
{
    public class ManejadorUsuarios : IManejadorUsuarios
    {
        IRepositorio<Usuario> repositorio;
        public ManejadorUsuarios(IRepositorio<Usuario> repositorio)
        {
            this.repositorio = repositorio;
        }
        public List<Usuario> Listar => repositorio.Read;

        public bool Agregar(Usuario entidad)
        {
            return repositorio.Create(entidad);
        }

        public Usuario BuscarPorId(string id)
        {
            return Listar.Where(e => e.Id == id).SingleOrDefault();
        }

        public int BuscarUsuario(string usuario, string password)
        {
            int valor = 0;
            foreach (var item in repositorio.Read)
            {
                if(item.Nombre==usuario&&item.Contrasenia==password)
                {
                    valor = 1;
                }
            }
            return valor;
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Usuario entidad)
        {
            return repositorio.Update(entidad);
        }
    }
}
