﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Categoria : Base
    {
        public string Nombre { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Nombre);
        }
    }
}
