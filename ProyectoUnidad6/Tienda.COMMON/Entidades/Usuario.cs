﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tienda.COMMON.Entidades
{
    public class Usuario:Base
    {
        public string Nombre { get; set; }
        public string Contrasenia { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Nombre);
        }
    }
}
