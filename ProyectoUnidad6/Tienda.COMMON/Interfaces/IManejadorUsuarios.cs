﻿using System;
using System.Collections.Generic;
using System.Text;
using Tienda.COMMON.Entidades;

namespace Tienda.COMMON.Interfaces
{
    public interface IManejadorUsuarios : IManejadorGenerico<Usuario>
    {
        int BuscarUsuario(string usuario, string password);
    }
}
