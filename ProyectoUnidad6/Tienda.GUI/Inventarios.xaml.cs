﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tienda.BIZ;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;
using Tienda.DAL;

namespace Tienda.GUI
{
    /// <summary>
    /// Lógica de interacción para Inventarios.xaml
    /// </summary>
    public partial class Inventarios : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorProductos manejadorProductos;
        IManejadorEmpleados manejadorEmpleados;
        IManejadorCategorias manejadorCategorias;
        accion accionEmpleados;
        accion accionProductos;
        accion accionCategorias;
        public Inventarios()
        {
            InitializeComponent();
            manejadorProductos = new ManejadorProductos(new RepositorioDeProductos());
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorCategorias = new ManejadorCategorias(new RepositorioDeCategorias());

            ActualizarCombosCategoria();

            PonerBotonesProductosEnEdicion(false);
            LimpiarCamposDeProductos();
            ActualizarTablaDeProductos();

            PonerBotonesEmpleadosEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();

            PonerBotonesCategoriasEnEdicion(false);
            LimpiarCamposDeCategorias();
            ActualizarTablaDeCategorias();
        }

        private void ActualizarCombosCategoria()
        {
            cmbProductosCategoria.ItemsSource = null;
            cmbProductosCategoria.ItemsSource = manejadorCategorias.Listar;
        }
        private void ActualizarTablaDeProductos()
        {
            dtgProductos.ItemsSource = null;
            dtgProductos.ItemsSource = manejadorProductos.Listar;
        }

        private void LimpiarCamposDeProductos()
        {
            cmbProductosCategoria.ItemsSource = null;
            txbProductosModelo.Clear();
            txbProductosNombre.Clear();
            txbProductosDescripcion.Clear();
            txbProductosId.Text = "";
            txbProductosPrecioCompra.Clear();
            txbProductosPrecioVenta.Clear();
            txbProductosMarca.Clear();
            imgFoto.Source = null;
        }

        private void PonerBotonesProductosEnEdicion(bool value)
        {
            btnProductosCancelar.IsEnabled = value;
            btnProductosEditar.IsEnabled = !value;
            btnProductosEliminar.IsEnabled = !value;
            btnProductosGuardar.IsEnabled = value;
            btnProductosNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = manejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadosApellidoPaterno.Clear();
            txbEmpleadosApellidoMaterno.Clear();
            txbEmpleadosId.Text = "";
            txbEmpleadosNombre.Clear();
            imgFotoEmpleado.Source = null;
        }

        private void PonerBotonesEmpleadosEnEdicion(bool value)
        {
            btnEmpleadosCancelar.IsEnabled = value;
            btnEmpleadosEditar.IsEnabled = !value;
            btnEmpleadosEliminar.IsEnabled = !value;
            btnEmpleadosGuardar.IsEnabled = value;
            btnEmpleadosNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaDeCategorias()
        {
            dtgCategorias.ItemsSource = null;
            dtgCategorias.ItemsSource = manejadorCategorias.Listar;
        }

        private void LimpiarCamposDeCategorias()
        {
            txbEmpleadosId.Text = "";
            txbCategoriasNombre.Clear();
        }

        private void PonerBotonesCategoriasEnEdicion(bool value)
        {
            btnCategoriasCancelar.IsEnabled = value;
            btnCategoriasEditar.IsEnabled = !value;
            btnCategoriasEliminar.IsEnabled = !value;
            btnCategoriasGuardar.IsEnabled = value;
            btnCategoriasNuevo.IsEnabled = !value;
        }

        private void btnProductosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            accionProductos = accion.Nuevo;
            PonerBotonesProductosEnEdicion(true);
            ActualizarCombosCategoria();
        }

        private void btnProductosEditar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarCombosCategoria();
            LimpiarCamposDeProductos();
            accionProductos = accion.Editar;
            PonerBotonesProductosEnEdicion(true);
            Producto p = dtgProductos.SelectedItem as Producto;
            if (p != null)
            {
                ActualizarCombosCategoria();
                cmbProductosCategoria.Text = p.Categoria;
                txbProductosModelo.Text = p.Modelo;
                txbProductosNombre.Text = p.Nombre;
                txbProductosDescripcion.Text = p.Descripcion;
                txbProductosPrecioCompra.Text = p.PrecioCompra;
                txbProductosPrecioVenta.Text = p.PrecioVenta;
                txbProductosMarca.Text = p.Marca;
                imgFoto.Source = ByteToImage(p.Fotografia);
            }
            ActualizarCombosCategoria();
        }

        private void btnProductosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionProductos == accion.Nuevo)
            {
                Producto p = new Producto()
                {
                    Categoria = cmbProductosCategoria.Text,
                    Nombre = txbProductosNombre.Text,
                    Descripcion = txbProductosDescripcion.Text,
                    PrecioCompra = txbProductosPrecioCompra.Text,
                    PrecioVenta = txbProductosPrecioVenta.Text,
                    Marca = txbProductosMarca.Text,
                    Modelo = txbProductosModelo.Text,
                    Fotografia = ImageToByte(imgFoto.Source)
                };
                if (string.IsNullOrEmpty(txbProductosNombre.Text) || string.IsNullOrEmpty(txbProductosDescripcion.Text) || string.IsNullOrEmpty(txbProductosPrecioCompra.Text) || string.IsNullOrEmpty(txbProductosPrecioVenta.Text) || string.IsNullOrEmpty(txbProductosMarca.Text) || string.IsNullOrEmpty(txbProductosModelo.Text) || string.IsNullOrEmpty(cmbProductosCategoria.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorProductos.Agregar(p))
                    {
                        MessageBox.Show("Producto agregado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeProductos();
                        ActualizarTablaDeProductos();
                        PonerBotonesProductosEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                
                Producto p = dtgProductos.SelectedItem as Producto;
                p.Nombre = txbProductosNombre.Text;
                p.Descripcion = txbProductosDescripcion.Text;
                p.PrecioCompra = txbProductosPrecioCompra.Text;
                p.PrecioVenta = txbProductosPrecioVenta.Text;
                p.Marca = txbProductosMarca.Text;
                p.Fotografia = ImageToByte(imgFoto.Source);
                p.Modelo = txbProductosModelo.Text;
                p.Categoria = cmbProductosCategoria.Text;
                if (manejadorProductos.Modificar(p))
                {
                    MessageBox.Show("Producto modificado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeProductos();
                    ActualizarTablaDeProductos();
                    PonerBotonesProductosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnProductosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeProductos();
            PonerBotonesProductosEnEdicion(false);
        }

        private void btnProductosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Producto p = dtgProductos.SelectedItem as Producto;
            if (p != null)
            {
                if (MessageBox.Show("¿Realmente desea eliminar este Producto?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorProductos.Eliminar(p.Id))
                    {
                        MessageBox.Show("Producto Eliminado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeProductos();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        public ImageSource ByteToImage(byte[] imageData)
        {
            if (imageData == null)
            {
                return null;
            }
            else
            {
                BitmapImage biImg = new BitmapImage();
                MemoryStream ms = new MemoryStream(imageData);
                biImg.BeginInit();
                biImg.StreamSource = ms;
                biImg.EndInit();
                ImageSource imgSrc = biImg as ImageSource;
                return imgSrc;
            }
        }

        public byte[] ImageToByte(ImageSource image)
        {
            if (image != null)
            {
                MemoryStream memStream = new MemoryStream();
                JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(image as BitmapSource));
                encoder.Save(memStream);
                return memStream.ToArray();
            }
            else
            {
                return null;
            }
        }
        private void btnCargarFoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialogo = new OpenFileDialog();
            dialogo.Title = "Selecciona una foto";
            dialogo.Filter = "Archivos de imagen|*.jpg; *.png; *.gif";
            if (dialogo.ShowDialog().Value)
            {
                imgFoto.Source = new BitmapImage(new Uri(dialogo.FileName));
            }
        }

        private void btnEmpleadosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(true);
            accionEmpleados = accion.Nuevo;
        }

        private void btnEmpleadosEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                txbEmpleadosId.Text = emp.Id;
                txbEmpleadosApellidoPaterno.Text = emp.ApellidoPaterno;
                txbEmpleadosApellidoMaterno.Text = emp.ApellidoMaterno;
                txbEmpleadosNombre.Text = emp.Nombre;
                imgFotoEmpleado.Source = ByteToImage(emp.Fotografia);
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadosEnEdicion(true);
            }
        }

        private void btnEmpleadosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    Nombre = txbEmpleadosNombre.Text,
                    ApellidoPaterno = txbEmpleadosApellidoPaterno.Text,
                    ApellidoMaterno = txbEmpleadosApellidoMaterno.Text,
                    Fotografia = ImageToByte(imgFotoEmpleado.Source)
                };
                if (string.IsNullOrEmpty(txbEmpleadosNombre.Text) || string.IsNullOrEmpty(txbEmpleadosApellidoPaterno.Text) || string.IsNullOrEmpty(txbEmpleadosApellidoMaterno.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorEmpleados.Agregar(emp))
                    {
                        MessageBox.Show("Empleado agregado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeEmpleados();
                        ActualizarTablaEmpleados();
                        PonerBotonesEmpleadosEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Empleado no se pudo agregar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Empleado emp = dtgEmpleados.SelectedItem as Empleado;
                emp.ApellidoPaterno = txbEmpleadosApellidoPaterno.Text;
                emp.ApellidoMaterno = txbEmpleadosApellidoMaterno.Text;
                emp.Nombre = txbEmpleadosNombre.Text;
                emp.Fotografia = ImageToByte(imgFotoEmpleado.Source);
                if (manejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Empleado no se pudo actualizar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEmpleadosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadosEnEdicion(false);
        }

        private void btnEmpleadosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCategoriasNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeCategorias();
            PonerBotonesCategoriasEnEdicion(true);
            accionCategorias = accion.Nuevo;
            ActualizarCombosCategoria();
        }

        private void btnCategoriasEditar_Click(object sender, RoutedEventArgs e)
        {
            Categoria c = dtgCategorias.SelectedItem as Categoria;
            if (c != null)
            {
                txbCategoriasId.Text = c.Id;
                txbCategoriasNombre.Text = c.Nombre;
                accionCategorias = accion.Editar;
                PonerBotonesCategoriasEnEdicion(true);
            }
        }

        private void btnCategoriasGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionCategorias == accion.Nuevo)
            {
                Categoria c = new Categoria()
                {
                    Nombre = txbCategoriasNombre.Text,
                };
                if (string.IsNullOrEmpty(txbCategoriasNombre.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorCategorias.Agregar(c))
                    {
                        MessageBox.Show("Categoria agregada correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeCategorias();
                        ActualizarTablaDeCategorias();
                        PonerBotonesCategoriasEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("La Categoria no se pudo agregar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Categoria c = dtgCategorias.SelectedItem as Categoria;
                c.Nombre = txbCategoriasNombre.Text;
                if (manejadorCategorias.Modificar(c))
                {
                    MessageBox.Show("Categoria modificada correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeCategorias();
                    ActualizarTablaDeCategorias();
                    PonerBotonesCategoriasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("La Categoria no se pudo actualizar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCategoriasCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeCategorias();
            PonerBotonesCategoriasEnEdicion(false);
        }

        private void btnCategoriasEliminar_Click(object sender, RoutedEventArgs e)
        {
            Categoria c = dtgCategorias.SelectedItem as Categoria;
            if (c != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar esta Categoria?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorCategorias.Eliminar(c.Id))
                    {
                        MessageBox.Show("Categoria eliminada", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeCategorias();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar la Categoria", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnCargarFotoEmpleado_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialogo = new OpenFileDialog();
            dialogo.Title = "Selecciona una foto";
            dialogo.Filter = "Archivos de imagen|*.jpg; *.png; *.gif";
            if (dialogo.ShowDialog().Value)
            {
                imgFotoEmpleado.Source = new BitmapImage(new Uri(dialogo.FileName));
            }
        }

        private void btnRegresarEmpleados_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Datos datos = new Datos();
            datos.Show();
        }

        private void btnRegresarProductos_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Datos datos = new Datos();
            datos.Show();
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Datos datos = new Datos();
            datos.Show();
        }
    }
}
