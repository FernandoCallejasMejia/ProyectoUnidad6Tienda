﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Tienda.BIZ;
using Tienda.COMMON.Interfaces;
using Tienda.DAL;

namespace Tienda.GUI
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IManejadorUsuarios manejadorUsuarios;
        public MainWindow()
        {
            InitializeComponent();
            Datos datos = new Datos();
            manejadorUsuarios = new ManejadorUsuarios(new RepositorioDeUsuarios());
            txbContrasenia.Visibility = Visibility.Collapsed;
            //SplashScreen splashScreen = new SplashScreen("Imagenes/Logo.gif");
            //splashScreen.Show(true);
            ActualizarCombosUsuario();
        }

        //private void _MenuExit_Click(object sender, RoutedEventArgs e)
        //{
        //    System.Windows.Application.Current.MainWindow.Close();
        //}

        //protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        //{
        //    MessageBoxResult result = MessageBox.Show("Realmente deseas salir", "", MessageBoxButton.OKCancel);
        //    if (result == MessageBoxResult.Cancel)
        //    {
        //        e.Cancel = true;
        //    }
        //    base.OnClosing(e);
        //}

        private void ActualizarCombosUsuario()
        {
            cmbUsuario.ItemsSource = null;
            cmbUsuario.ItemsSource = manejadorUsuarios.Listar;
        }

        private void btnEntrar_Click(object sender, RoutedEventArgs e)
        {
            Datos datos = new Datos();
            if (string.IsNullOrEmpty(cmbUsuario.Text)||string.IsNullOrEmpty(psbContrasenia.Password))
            {
                MessageBox.Show("Faltan Usuario o Contraseña", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            if (manejadorUsuarios.BuscarUsuario(cmbUsuario.Text, psbContrasenia.Password) == 1)
            {
                MessageBox.Show("Acceso Correcto", "Bienvenido");
                datos.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Contraseña o Usuario incorrecto", "Error");
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            txbContrasenia.Text = "";
            cmbUsuario.Text = "";
        }

        private void Visualizador_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(psbContrasenia.Password))
            {
                MessageBox.Show("Falta la contraseña", "Error");
                return;
            }
            if (txbContrasenia.Visibility==Visibility.Collapsed)
            {
                psbContrasenia.Visibility = Visibility.Collapsed;
                txbContrasenia.Text = psbContrasenia.Password;
                txbContrasenia.Visibility = Visibility.Visible;
            }
            else
            {
                psbContrasenia.Visibility = Visibility.Visible;
                txbContrasenia.Visibility = Visibility.Collapsed;
            }
        }
    }
}
