﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tienda.BIZ;
using Tienda.COMMON.Entidades;
using Tienda.COMMON.Interfaces;
using Tienda.DAL;

namespace Tienda.GUI
{
    /// <summary>
    /// Lógica de interacción para VUsuarios.xaml
    /// </summary>
    public partial class VUsuarios : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorUsuarios manejadorUsuarios;
        accion accionUsuarios;
        public VUsuarios()
        {
            InitializeComponent();
            manejadorUsuarios = new ManejadorUsuarios(new RepositorioDeUsuarios());

            PonerBotonesUsuariosEnEdicion(false);
            LimpiarCamposDeUsuarios();
            ActualizarTablaDeUsuarios();
        }

        private void ActualizarTablaDeUsuarios()
        {
            dtgUsuarios.ItemsSource = null;
            dtgUsuarios.ItemsSource = manejadorUsuarios.Listar;
        }

        private void LimpiarCamposDeUsuarios()
        {
            txbUsuariosContrasenia.Clear();
            txbUsuariosNombre.Clear();
            txbUsuariosId.Text = "";
        }

        private void PonerBotonesUsuariosEnEdicion(bool value)
        {
            btnUsuariosCancelar.IsEnabled = value;
            btnUsuariosEditar.IsEnabled = !value;
            btnUsuariosEliminar.IsEnabled = !value;
            btnUsuariosGuardar.IsEnabled = value;
            btnUsuariosNuevo.IsEnabled = !value;
        }

        private void btnUsuariosNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeUsuarios();
            PonerBotonesUsuariosEnEdicion(true);
            accionUsuarios = accion.Nuevo;
        }

        private void btnUsuariosEditar_Click(object sender, RoutedEventArgs e)
        {
            Usuario u = dtgUsuarios.SelectedItem as Usuario;
            if (u != null)
            {
                txbUsuariosId.Text = u.Id;
                txbUsuariosContrasenia.Text = u.Contrasenia;
                txbUsuariosNombre.Text = u.Nombre;
                accionUsuarios = accion.Editar;
                PonerBotonesUsuariosEnEdicion(true);
            }
        }

        private void btnUsuariosGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionUsuarios == accion.Nuevo)
            {
                Usuario u = new Usuario()
                {
                    Nombre = txbUsuariosNombre.Text,
                    Contrasenia = txbUsuariosContrasenia.Text,
                };
                if (string.IsNullOrEmpty(txbUsuariosNombre.Text) || string.IsNullOrEmpty(txbUsuariosContrasenia.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorUsuarios.Agregar(u))
                    {
                        MessageBox.Show("Usuario agregado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeUsuarios();
                        ActualizarTablaDeUsuarios();
                        PonerBotonesUsuariosEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Usuario no se pudo agregar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Usuario u = dtgUsuarios.SelectedItem as Usuario;
                u.Contrasenia = txbUsuariosContrasenia.Text;
                u.Nombre = txbUsuariosNombre.Text;
                if (manejadorUsuarios.Modificar(u))
                {
                    MessageBox.Show("Usuario modificado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeUsuarios();
                    ActualizarTablaDeUsuarios();
                    PonerBotonesUsuariosEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Usuario no se pudo actualizar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnUsuariosCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeUsuarios();
            PonerBotonesUsuariosEnEdicion(false);
        }

        private void btnUsuariosEliminar_Click(object sender, RoutedEventArgs e)
        {
            Usuario u = dtgUsuarios.SelectedItem as Usuario;
            if (u != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Usuario?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorUsuarios.Eliminar(u.Id))
                    {
                        MessageBox.Show("Usuario eliminado", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeUsuarios();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el usuario", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            Datos datos = new Datos();
            datos.Show();
        }
    }
}
