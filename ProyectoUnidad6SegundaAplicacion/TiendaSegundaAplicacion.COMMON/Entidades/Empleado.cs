﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaSegundaAplicacion.COMMON.Entidades
{
    public class Empleado: Base
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public byte[] Fotografia { get; set; }
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Nombre, ApellidoPaterno, ApellidoMaterno);
        }
    }
}
