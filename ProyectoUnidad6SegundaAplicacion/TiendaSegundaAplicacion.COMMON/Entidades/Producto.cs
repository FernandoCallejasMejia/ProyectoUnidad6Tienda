﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaSegundaAplicacion.COMMON.Entidades
{
    public class Producto: Base
    {
        public int Cantidad { get; set; }
        public string Nombre { get; set; }
        public string Categoria { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Total { get; set; }
        public string Descripcion { get; set; }
        public string PrecioCompra { get; set; }
        public string PrecioVenta { get; set; }
        public override string ToString()
        {
            return string.Format("{0} ${1}", Nombre,PrecioVenta);
        }
    }
}
