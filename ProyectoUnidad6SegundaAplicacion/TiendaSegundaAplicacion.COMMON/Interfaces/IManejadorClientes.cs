﻿using System;
using System.Collections.Generic;
using System.Text;
using TiendaSegundaAplicacion.COMMON.Entidades;

namespace TiendaSegundaAplicacion.COMMON.Interfaces
{
    public interface IManejadorClientes : IManejadorGenerico<Cliente>
    {
    }
}
