﻿using System;
using System.Collections.Generic;
using System.Text;
using TiendaSegundaAplicacion.COMMON.Entidades;

namespace TiendaSegundaAplicacion.COMMON.Modelos
{
    public class ModelTicketsParaReporte
    {
        public string NombreCliente { get; set; }
        public string NombreEmpleado { get; set; }
        public DateTime FechaHoraVenta { get; set; }
        public int Cantidad { get; set; }
        public ModelTicketsParaReporte(Ticket ticket)
        {
            NombreCliente = string.Format("{0}", ticket.Comprador.Nombre);
            NombreEmpleado = string.Format("{0} {1} {2}", ticket.Vendedor.Nombre, ticket.Vendedor.ApellidoPaterno, ticket.Vendedor.ApellidoMaterno);
            FechaHoraVenta = ticket.FechaHoraVenta;
            Cantidad = ticket.ProductosVendidos.Count;
        }
    }
}
