﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TiendaSegundaAplicacion.BIZ;
using TiendaSegundaAplicacion.COMMON.Entidades;
using TiendaSegundaAplicacion.COMMON.Interfaces;
using TiendaSegundaAplicacion.DAL;

namespace TiendaSegundaAplicacion.GUI
{
    /// <summary>
    /// Lógica de interacción para Clientes.xaml
    /// </summary>
    public partial class Clientes : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorClientes manejadorClientes;
        accion accionClientes;
        public Clientes()
        {
            InitializeComponent();
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());

            PonerBotonesClientesEnEdicion(false);
            LimpiarCamposDeClientes();
            ActualizarTablaDeClientes();
        }

        private void ActualizarTablaDeClientes()
        {
            dtgClientes.ItemsSource = null;
            dtgClientes.ItemsSource = manejadorClientes.Listar;
        }

        private void LimpiarCamposDeClientes()
        {
            txbClientesTelefono.Clear();
            txbClientesNombre.Clear();
            txbClientesId.Text = "";
        }

        private void PonerBotonesClientesEnEdicion(bool value)
        {
            btnClientesCancelar.IsEnabled = value;
            btnClientesEditar.IsEnabled = !value;
            btnClientesEliminar.IsEnabled = !value;
            btnClientesGuardar.IsEnabled = value;
            btnClientesNuevo.IsEnabled = !value;
        }
        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Close();
        }

        private void btnClientesEliminar_Click(object sender, RoutedEventArgs e)
        {
            Cliente c = dtgClientes.SelectedItem as Cliente;
            if (c != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este Cliente?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorClientes.Eliminar(c.Id))
                    {
                        MessageBox.Show("Cliente eliminado", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaDeClientes();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el Cliente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnClientesCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(false);
        }

        private void btnClientesGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionClientes == accion.Nuevo)
            {
                Cliente c = new Cliente()
                {
                    Nombre = txbClientesNombre.Text,
                    Telefono = txbClientesTelefono.Text,
                };
                if (string.IsNullOrEmpty(txbClientesNombre.Text) || string.IsNullOrEmpty(txbClientesTelefono.Text))
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    if (manejadorClientes.Agregar(c))
                    {
                        MessageBox.Show("Cliente agregado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                        LimpiarCamposDeClientes();
                        ActualizarTablaDeClientes();
                        PonerBotonesClientesEnEdicion(false);
                    }
                    else
                    {
                        MessageBox.Show("El Cliente no se pudo agregar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                Cliente c = dtgClientes.SelectedItem as Cliente;
                c.Telefono = txbClientesTelefono.Text;
                c.Nombre = txbClientesNombre.Text;
                if (manejadorClientes.Modificar(c))
                {
                    MessageBox.Show("Cliente modificado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeClientes();
                    ActualizarTablaDeClientes();
                    PonerBotonesClientesEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El Cliente no se pudo actualizar", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnClientesEditar_Click(object sender, RoutedEventArgs e)
        {
            Cliente c = dtgClientes.SelectedItem as Cliente;
            if (c != null)
            {
                txbClientesId.Text = c.Id;
                txbClientesTelefono.Text =  c.Telefono;
                txbClientesNombre.Text = c.Nombre;
                accionClientes = accion.Editar;
                PonerBotonesClientesEnEdicion(true);
            }
        }

        private void btnClientesNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeClientes();
            PonerBotonesClientesEnEdicion(true);
            accionClientes = accion.Nuevo;
        }
    }
}
