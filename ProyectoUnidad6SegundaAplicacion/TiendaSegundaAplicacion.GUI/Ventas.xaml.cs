﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TiendaSegundaAplicacion.BIZ;
using TiendaSegundaAplicacion.COMMON.Entidades;
using TiendaSegundaAplicacion.COMMON.Interfaces;
using TiendaSegundaAplicacion.COMMON.Modelos;
using TiendaSegundaAplicacion.DAL;

namespace TiendaSegundaAplicacion.GUI
{
    /// <summary>
    /// Lógica de interacción para Ventas.xaml
    /// </summary>
    public partial class Ventas : Window
    {
        enum Accion
        {
            Nuevo,
            Editar
        }
        IManejadorTickets manejadorDeTickets;
        IManejadorEmpleados manejadorEmpleados;
        IManejadorProductos manejadorProductos;
        IManejadorClientes manejadorClientes;
        Ticket ticket;

        Accion accionTicket;

        public string total { get; private set; }

        public Ventas()
        {
            InitializeComponent();

            manejadorDeTickets = new ManejadorTickets(new RepositorioDeTickets());
            manejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorProductos = new ManejadorProductos(new RepositorioDeProductos());
            manejadorClientes = new ManejadorClientes(new RepositorioDeClientes());

            ActualizarTablaDeTickets();
            gridVenta.IsEnabled = false;
        }

        private void ActualizarTablaDeTickets()
        {
            dtgTickets.ItemsSource = null;
            dtgTickets.ItemsSource = manejadorDeTickets.Listar;
        }

        private void ActualizarListaDeProductosEnTicket()
        {
            dtgProductosEnTicket.ItemsSource = null;
            dtgProductosEnTicket.ItemsSource = ticket.ProductosVendidos;
        }
        private void ActualizarCombosVenta()
        {
            cmbEmpleado.ItemsSource = null;
            cmbEmpleado.ItemsSource = manejadorEmpleados.Listar;

            cmbProductos.ItemsSource = null;
            cmbProductos.ItemsSource = manejadorProductos.Listar;

            cmbCliente.ItemsSource = null;
            cmbCliente.ItemsSource = manejadorClientes.Listar;
        }
        private void LimpiarCamposDeTicket()
        {
            lblFechaHoraVenta.Content = "";
            dtgProductosEnTicket.ItemsSource = null;
            cmbEmpleado.ItemsSource = null;
            cmbCliente.ItemsSource = null;
            cmbProductos.ItemsSource = null;
            nudCantidad.Text = null;
            txbTotal.Text = null;
        }

        private void btnGenerarTicket_Click(object sender, RoutedEventArgs e)
        {
            if (accionTicket == Accion.Nuevo)
            {
                ticket.Vendedor = cmbEmpleado.SelectedItem as Empleado;
                ticket.Comprador = cmbCliente.SelectedItem as Cliente;
                ticket.FechaHoraVenta = DateTime.Now;
                ticket.Cantidad = int.Parse(nudCantidad.Text);
                ticket.Total = float.Parse(txbTotal.Text);
                lblFechaHoraVenta.Content = DateTime.Now;
                ArchivoTicket venta = new ArchivoTicket();
                if (venta.GenerarTicket(cmbCliente.Text, cmbProductos.Text, int.Parse(nudCantidad.Text), float.Parse(txbTotal.Text), cmbEmpleado.Text, cmbCliente.Text + ".txt"))
                {
                    if (manejadorDeTickets.Agregar(ticket))
                    {

                        MessageBox.Show("Ticket generado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        LimpiarCamposDeTicket();
                        gridVenta.IsEnabled = false;
                        ActualizarTablaDeTickets();
                    }
                    else
                    {
                        MessageBox.Show("Error al generar el Ticket", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                ticket.Vendedor = cmbEmpleado.SelectedItem as Empleado;
                ticket.Comprador = cmbCliente.SelectedItem as Cliente;
                ticket.FechaHoraVenta = DateTime.Now;
                ticket.Cantidad = int.Parse(nudCantidad.Text);
                ticket.Total = float.Parse(txbTotal.Text);
                lblFechaHoraVenta.Content = DateTime.Now;
                if (manejadorDeTickets.Modificar(ticket))
                {
                    MessageBox.Show("Ticket generado correctamente", "Tienda", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeTicket();
                    gridVenta.IsEnabled = false;
                    ActualizarTablaDeTickets();
                }
                else
                {
                    MessageBox.Show("Error al generar el Ticket", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCalcularTotal_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(nudCantidad.Text)|| string.IsNullOrEmpty(cmbEmpleado.Text) || string.IsNullOrEmpty(cmbCliente.Text) || string.IsNullOrEmpty(cmbProductos.Text))
            {
                MessageBox.Show("Faltan datos", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show("Listo", "Venta", MessageBoxButton.OK, MessageBoxImage.Information);
                Producto p = cmbProductos.SelectedItem as Producto;
                Ticket venta = new Ticket();
                p.Cantidad= int.Parse(nudCantidad.Text);
                p.Total= (int.Parse(nudCantidad.Text)*float.Parse(p.PrecioVenta)).ToString();
                btnGenerarTicket.IsEnabled = true;
                if (p != null)
                {
                    //ticket.ProductosVendidos.Add(p);
                    //ActualizarListaDeProductosEnTicket();
                    //for (int i = 0; i < ticket.ProductosVendidos.Count; i++)
                    //{
                    //    total = (p.Cantidad * float.Parse(p.PrecioVenta)).ToString();
                    //    total = total + total;
                    //    txbTotal.Text = total;
                    //}
                    float total = 0;
                    int cantidad = 0;
                    foreach (Producto item in ticket.ProductosVendidos)
                    {
                        total += float.Parse(item.Total);
                        cantidad += item.Cantidad;
                        txbTotal.Text = total.ToString();
                        venta.Cantidad = cantidad;
                    }

                }
            }
        }

        private void btnCancelarTicket_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeTicket();
            gridVenta.IsEnabled = false;
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            gridVenta.IsEnabled = true;
            ActualizarCombosVenta();
            btnGenerarTicket.IsEnabled = false;
            ticket = new Ticket();
            ticket.ProductosVendidos = new List<Producto>();
            ActualizarListaDeProductosEnTicket();
            accionTicket = Accion.Nuevo;
        }

        private void btnEliminarTicket_Click(object sender, RoutedEventArgs e)
        {
            Ticket t = dtgTickets.SelectedItem as Ticket;
            if (t != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar el registro del Ticket?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeTickets.Eliminar(t.Id))
                    {
                        MessageBox.Show("Eliminado con éxito", "Tienda", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeTickets();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnAgregarPoducto_Click(object sender, RoutedEventArgs e)
        {
            Producto p = cmbProductos.SelectedItem as Producto;
            p.Cantidad = int.Parse(nudCantidad.Text);
            p.Total = (int.Parse(nudCantidad.Text) * float.Parse(p.PrecioVenta)).ToString();
            if (p != null)
            {
                ticket.ProductosVendidos.Add(p);
                ActualizarListaDeProductosEnTicket();
                //equipos.Add(eq);
            }
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            List<ReportDataSource> datos = new List<ReportDataSource>();
            ReportDataSource tickets = new ReportDataSource();
            List<ModelTicketsParaReporte> ticketsporentregar = new List<ModelTicketsParaReporte>();
            foreach (var item in manejadorDeTickets.TicketsEntregados())
            {
                ticketsporentregar.Add(new ModelTicketsParaReporte(item));
            }
            tickets.Value = ticketsporentregar;
            tickets.Name = "Tickets";
            datos.Add(tickets);
            Reporteador ventana = new Reporteador("TiendaSegundaAplicacion.GUI.Reportes.SinParametros.rdlc", datos);
            ventana.ShowDialog();
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            Menu menu = new Menu();
            menu.Show();
            this.Close();
        }

        private void ExportToExcelAndCsv()
        {
            dtgProductosEnTicket.SelectAllCells();
            dtgProductosEnTicket.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, dtgProductosEnTicket);
            String resultant = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            String result = (string)Clipboard.GetData(DataFormats.Text);
            dtgProductosEnTicket.UnselectAllCells();
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(@"C:\Tienda\DatosExcel/ticket.xls");
            file1.WriteLine(result.Replace(',', ' '));
            file1.Close();
            MessageBox.Show("Exportacion de datos a Excel realizada con exito");
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {
            ExportToExcelAndCsv();
        }

    }
}