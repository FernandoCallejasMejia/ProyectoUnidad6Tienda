﻿#pragma checksum "..\..\Clientes.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4337B1F57A7B9D3118E23BA3DD6B54BAEBC76548"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using TiendaSegundaAplicacion.GUI;


namespace TiendaSegundaAplicacion.GUI {
    
    
    /// <summary>
    /// Clientes
    /// </summary>
    public partial class Clientes : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRegresar;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClientesNuevo;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClientesEditar;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClientesGuardar;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClientesCancelar;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnClientesEliminar;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txbClientesId;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbClientesNombre;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txbClientesTelefono;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\Clientes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgClientes;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TiendaSegundaAplicacion.GUI;component/clientes.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Clientes.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnRegresar = ((System.Windows.Controls.Button)(target));
            
            #line 18 "..\..\Clientes.xaml"
            this.btnRegresar.Click += new System.Windows.RoutedEventHandler(this.btnRegresar_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnClientesNuevo = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\Clientes.xaml"
            this.btnClientesNuevo.Click += new System.Windows.RoutedEventHandler(this.btnClientesNuevo_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnClientesEditar = ((System.Windows.Controls.Button)(target));
            
            #line 24 "..\..\Clientes.xaml"
            this.btnClientesEditar.Click += new System.Windows.RoutedEventHandler(this.btnClientesEditar_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnClientesGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\Clientes.xaml"
            this.btnClientesGuardar.Click += new System.Windows.RoutedEventHandler(this.btnClientesGuardar_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btnClientesCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 30 "..\..\Clientes.xaml"
            this.btnClientesCancelar.Click += new System.Windows.RoutedEventHandler(this.btnClientesCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnClientesEliminar = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\Clientes.xaml"
            this.btnClientesEliminar.Click += new System.Windows.RoutedEventHandler(this.btnClientesEliminar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txbClientesId = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.txbClientesNombre = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txbClientesTelefono = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.dtgClientes = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

