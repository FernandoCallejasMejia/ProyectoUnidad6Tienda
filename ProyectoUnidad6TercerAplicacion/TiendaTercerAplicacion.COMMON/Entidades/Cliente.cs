﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaTercerAplicacion.COMMON.Entidades
{
    public class Cliente
    {
        public string Nombre { get; set; }
        public string Telefono { get; set; }
        public override string ToString()
        {
            return string.Format("{0}", Nombre);
        }
    }
}
