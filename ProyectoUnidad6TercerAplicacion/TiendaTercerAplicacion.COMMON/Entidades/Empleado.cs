﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaTercerAplicacion.COMMON.Entidades
{
    public class Empleado
    {
        public string Nombre { get; set; }
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        public byte[] Fotografia { get; set; }
    }
}
