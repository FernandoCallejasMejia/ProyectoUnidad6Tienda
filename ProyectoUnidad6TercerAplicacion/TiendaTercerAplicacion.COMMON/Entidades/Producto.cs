﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaTercerAplicacion.COMMON.Entidades
{
    public class Producto: Base
    {
        public int Cantidad { get; set; }
        public string Nombre { get; set; }
        public string Categoria { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Descripcion { get; set; }
        public string PrecioCompra { get; set; }
        public string PrecioVenta { get; set; }
    }
}
