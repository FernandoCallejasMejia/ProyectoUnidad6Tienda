﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaTercerAplicacion.COMMON.Entidades
{
    public class Ticket:Base
    {
        public int Cantidad { get; set; }
        public float Total { get; set; }
        public DateTime FechaHoraVenta { get; set; }
        public List<Producto> ProductosVendidos { get; set; }
        public Empleado Vendedor { get; set; }
        public Cliente Comprador { get; set; }
    }
}
