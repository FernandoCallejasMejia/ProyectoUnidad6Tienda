﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TiendaTercerAplicacion.COMMON.Entidades
{
    public class TicketLista
    {
        public int X { get; set; }
        public string TicketGrafica { get; set; }
        public float Total { get; set; }
    }
}
