﻿using System;
using System.Collections.Generic;
using System.Text;
using TiendaTercerAplicacion.COMMON.Entidades;

namespace TiendaTercerAplicacion.COMMON.Graficos
{
    public class Generadora
    {
        public Generadora()
        {
            Puntos = new List<Puntos>();
        }

        public List<Puntos> Puntos { get; set; }

        public List<Puntos> GeneradorDatos(List<TicketLista> listaticket, double limiteInferior, double limiteSuperior, double incremento)
        {
            Puntos = new List<Puntos>();
            double contador = 1;
            foreach (var item in listaticket)
            {
                Puntos.Add(new Puntos(contador++, item.Total));
            }
            return Puntos;
        }
    }
}
