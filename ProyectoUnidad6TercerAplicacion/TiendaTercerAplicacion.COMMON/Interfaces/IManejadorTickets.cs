﻿using System;
using System.Collections.Generic;
using System.Text;
using TiendaTercerAplicacion.COMMON.Entidades;

namespace TiendaTercerAplicacion.COMMON.Interfaces
{
    public interface IManejadorTickets : IManejadorGenerico<Ticket>
    {
        List<Ticket> TicketsEntregados();
    }
}
