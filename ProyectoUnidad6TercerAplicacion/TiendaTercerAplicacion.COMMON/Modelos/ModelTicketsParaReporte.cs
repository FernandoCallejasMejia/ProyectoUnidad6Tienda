﻿using System;
using System.Collections.Generic;
using System.Text;
using TiendaTercerAplicacion.COMMON.Entidades;

namespace TiendaTercerAplicacion.COMMON.Modelos
{
    public class ModelTicketsParaReporte
    {
        public string NombreCliente { get; set; }
        public string NombreEmpleado { get; set; }
        public string Total { get; set; }
        public DateTime FechaHoraVenta { get; set; }
        public int Cantidad { get; set; }
        public ModelTicketsParaReporte(Ticket ticket)
        {
            NombreCliente = string.Format("{0}", ticket.Comprador.Nombre);
            NombreEmpleado = string.Format("{0} {1} {2}", ticket.Vendedor.Nombre, ticket.Vendedor.ApellidoPaterno, ticket.Vendedor.ApellidoMaterno);
            FechaHoraVenta = ticket.FechaHoraVenta;
            Total = string.Format("{0}", ticket.Total);
            Cantidad = ticket.ProductosVendidos.Count;
        }
    }
}
