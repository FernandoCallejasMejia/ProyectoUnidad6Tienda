﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TiendaTercerAplicacion.COMMON.Entidades;
using TiendaTercerAplicacion.COMMON.Interfaces;

namespace TiendaTercerAplicacion.DAL
{
    public class RepositorioDeTickets : IRepositorio<Ticket>
    {
        private string DBName = @"C:\Tienda\Tienda.db";
        private string TableName = "Tickets";

        public List<Ticket> Read
        {
            get
            {
                List<Ticket> datos = new List<Ticket>();
                using (var db = new LiteDatabase(DBName))
                {
                    datos = db.GetCollection<Ticket>(TableName).FindAll().ToList();
                }
                return datos;
            }
        }

        public bool Create(Ticket entidad)
        {
            entidad.Id = Guid.NewGuid().ToString();
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ticket>(TableName);
                    coleccion.Insert(entidad);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                int r;
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ticket>(TableName);
                    r = coleccion.Delete(e => e.Id == id);
                }
                return r > 0;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(Ticket entidadModificada)
        {
            try
            {
                using (var db = new LiteDatabase(DBName))
                {
                    var coleccion = db.GetCollection<Ticket>(TableName);
                    coleccion.Update(entidadModificada);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
