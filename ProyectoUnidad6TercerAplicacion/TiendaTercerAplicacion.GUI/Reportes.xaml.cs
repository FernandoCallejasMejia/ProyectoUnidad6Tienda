﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TiendaTercerAplicacion.BIZ;
using TiendaTercerAplicacion.COMMON.Entidades;
using TiendaTercerAplicacion.COMMON.Interfaces;
using TiendaTercerAplicacion.COMMON.Modelos;
using TiendaTercerAplicacion.DAL;

namespace TiendaTercerAplicacion.GUI
{
    /// <summary>
    /// Lógica de interacción para Reportes.xaml
    /// </summary>
    public partial class Reportes : Window
    {
        IManejadorTickets manejadorDeTickets;
        Ticket ticket;
        public Reportes()
        {
            InitializeComponent();
            manejadorDeTickets = new ManejadorTickets(new RepositorioDeTickets());
            ActualizarTablaDeTickets();
        }

        private void ActualizarTablaDeTickets()
        {
            dtgTickets.ItemsSource = null;
            dtgTickets.ItemsSource = manejadorDeTickets.Listar;
        }

        private void btnEliminarTicket_Click(object sender, RoutedEventArgs e)
        {
            Ticket t = dtgTickets.SelectedItem as Ticket;
            if (t != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar el registro del Ticket?", "Tienda", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeTickets.Eliminar(t.Id))
                    {
                        MessageBox.Show("Eliminado con éxito", "Tienda", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeTickets();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Tienda", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            List<ReportDataSource> datos = new List<ReportDataSource>();
            ReportDataSource tickets = new ReportDataSource();
            List<ModelTicketsParaReporte> ticketsporentregar = new List<ModelTicketsParaReporte>();
            foreach (var item in manejadorDeTickets.TicketsEntregados())
            {
                ticketsporentregar.Add(new ModelTicketsParaReporte(item));
            }
            tickets.Value = ticketsporentregar;
            tickets.Name = "Tickets";
            datos.Add(tickets);
            Reporteador ventana = new Reporteador("TiendaTercerAplicacion.GUI.Reportes.SinParametros.rdlc", datos);
            ventana.ShowDialog();
        }

        private void btnRegresar_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();
            this.Close();
        }
        
    }
}
